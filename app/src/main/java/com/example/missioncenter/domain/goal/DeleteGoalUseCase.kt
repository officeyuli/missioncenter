package com.example.missioncenter.domain.goal

import com.example.missioncenter.data.repository.goal.GoalInteractor
import com.example.missioncenter.data.source.local.goal.Goal
import javax.inject.Inject

class DeleteGoalUseCase @Inject constructor(
    private val repo: GoalInteractor
) {
    operator fun invoke (goal: Goal): Int {
        return repo.deleteLocalGoal(goal)
    }
}