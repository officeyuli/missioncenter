package com.example.missioncenter.domain.goal

import androidx.lifecycle.LiveData
import com.example.missioncenter.data.repository.goal.GoalInteractor
import com.example.missioncenter.data.source.local.goal.Goal
import javax.inject.Inject

class GetGoalListUseCase @Inject constructor(
    private val repo: GoalInteractor
) {
    operator fun invoke (): LiveData<List<Goal>> {
        return repo.getLocalGoalList()
    }
}