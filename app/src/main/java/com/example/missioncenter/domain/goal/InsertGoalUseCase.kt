package com.example.missioncenter.domain.goal

import com.example.missioncenter.data.repository.goal.GoalInteractor
import com.example.missioncenter.data.source.local.goal.Goal
import io.reactivex.Single
import javax.inject.Inject

class InsertGoalUseCase @Inject constructor(
    private val repo: GoalInteractor
) {
    operator fun invoke (goal: Goal): Single<Long> {
        return repo.insertLocalGoal(goal)
    }
}