package com.example.missioncenter.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.missioncenter.data.source.local.goal.Goal
import com.example.missioncenter.data.source.local.goal.GoalDao

@Database(entities = [
    Goal::class
],version = 1, exportSchema = false)
abstract class MissionDatabase : RoomDatabase() {
    abstract fun gaolDao() : GoalDao
}