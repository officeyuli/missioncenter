package com.example.missioncenter.data.repository.goal

import androidx.lifecycle.LiveData
import com.example.missioncenter.data.source.local.goal.Goal
import com.example.missioncenter.data.source.local.goal.GoalDataSource
import io.reactivex.Single
import javax.inject.Inject

class GoalRepository @Inject internal constructor(
    private val goalDataSource: GoalDataSource
): GoalInteractor {
    override fun getLocalGoalList(): LiveData<List<Goal>> {
        return goalDataSource.getAll()
    }

    override fun getLocalGoal(id: Int): LiveData<Goal> {
        return goalDataSource.getGoal(id)
    }

    override fun insertLocalGoal(goal: Goal): Single<Long> {
        return goalDataSource.insertGaol(goal)
    }

    override fun deleteLocalGoal(goal: Goal): Int {
        return goalDataSource.deleteGaol(goal)
    }
}