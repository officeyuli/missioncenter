package com.example.missioncenter.data.source.local.goal

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.missioncenter.data.source.local.goal.Goal
import io.reactivex.Single
/*
UPDATE or DELETE queries can return void or int.
If it is an int, the value is the number of rows affected by this query.
 */
@Dao
interface GoalDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(gaol: Goal): Single<Long>

    @Query("select * from goal")
    fun getAll():LiveData<List<Goal>>

    @Query("SELECT * FROM goal WHERE id = :id")
    fun getGoal(id: Int): LiveData<Goal>

    @Update
    fun update(goal: Goal): Int

    @Delete
    fun delete(goal: Goal): Int
}