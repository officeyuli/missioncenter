package com.example.missioncenter.data.source.local.goal

import androidx.lifecycle.LiveData
import io.reactivex.Single

interface GoalDataSource {
    fun insertGaol(goal: Goal): Single<Long>
    fun updateGaol(goal: Goal): Int
    fun getGoal(id: Int): LiveData<Goal>
    fun getAll():LiveData<List<Goal>>
    fun deleteGaol(goal: Goal): Int
}