package com.example.missioncenter.data.repository.goal

import androidx.lifecycle.LiveData
import com.example.missioncenter.data.source.local.goal.Goal
import io.reactivex.Single

interface GoalInteractor {
    fun getLocalGoalList(): LiveData<List<Goal>>
    fun getLocalGoal(id:Int): LiveData<Goal>
    fun insertLocalGoal(goal: Goal): Single<Long>
    fun deleteLocalGoal(goal: Goal): Int
}