package com.example.missioncenter.data.source.local.goal


import androidx.lifecycle.LiveData
import io.reactivex.Single

class GoalLocalDataSource internal constructor(
    private val goalDao: GoalDao
): GoalDataSource {
    override fun insertGaol(goal: Goal): Single<Long> {
        return goalDao.insert(goal)
    }
    override fun updateGaol(goal: Goal): Int {
        return goalDao.update(goal)
    }
    override fun getGoal(id: Int): LiveData<Goal> {
        return goalDao.getGoal(id)
    }
    override fun getAll(): LiveData<List<Goal>> {
        return goalDao.getAll()
    }
    override fun deleteGaol(goal: Goal): Int {
        return goalDao.delete(goal)
    }
}
