package com.example.missioncenter.data.source.local.goal

import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Goal")
class Goal (
    @NonNull
    var target: String,
    @Nullable
    var deadline:String,
    @Nullable
    var difficulty:Int,
    @NonNull
    var available: Boolean
){
    @PrimaryKey(autoGenerate = true)
    var id:Int = 0
}