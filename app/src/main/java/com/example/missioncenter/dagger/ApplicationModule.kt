package com.example.missioncenter.dagger

import android.content.Context
import androidx.room.Room
import com.example.missioncenter.data.MissionDatabase
import com.example.missioncenter.data.repository.goal.GoalInteractor
import com.example.missioncenter.data.repository.goal.GoalRepository
import com.example.missioncenter.data.source.local.goal.GoalDataSource
import com.example.missioncenter.data.source.local.goal.GoalLocalDataSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object ApplicationModule {

    @JvmStatic
    @Singleton
    @Provides
    fun provideDataBase(context: Context): MissionDatabase {
        return Room.databaseBuilder(
            context.applicationContext,
            MissionDatabase::class.java,
            "MissionCenter.db"
        ).build()
    }
    @JvmStatic
    @Singleton
    @Provides
    fun provideGaolLocalDataSource(
        database: MissionDatabase
    ): GoalDataSource {
        return GoalLocalDataSource(
            database.gaolDao()
        )
    }

    @JvmStatic
    @Singleton
    @Provides
    fun provideGoalRepository(
        goalDataSource: GoalDataSource
    ): GoalInteractor {
        return GoalRepository(goalDataSource)
    }

}