package com.example.missioncenter.ui.main

import androidx.lifecycle.ViewModel
import com.example.missioncenter.domain.goal.DeleteGoalUseCase
import com.example.missioncenter.domain.goal.GetGoalListUseCase
import com.example.missioncenter.domain.goal.GetGoalUseCase
import com.example.missioncenter.domain.goal.InsertGoalUseCase
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val getGoalListUseCase: GetGoalListUseCase,
    private val getGoalUseCase: GetGoalUseCase,
    private val insetGoalUseCase: InsertGoalUseCase,
    private val deleteGoalUseCase: DeleteGoalUseCase
): ViewModel() {

}
