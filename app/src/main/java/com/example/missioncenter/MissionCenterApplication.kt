package com.example.missioncenter

import com.example.missioncenter.dagger.DaggerApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import timber.log.Timber

open class MissionCenterApplication : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {

        return DaggerApplicationComponent.factory().create(applicationContext)
    }

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
        if (BuildConfig.DEBUG)
            Timber.plant(Timber.DebugTree())
    }

    companion object{

        private var INSTANCE: MissionCenterApplication? = null

        @JvmStatic
        fun getInstance(): MissionCenterApplication{
            return INSTANCE!!
        }
    }
}